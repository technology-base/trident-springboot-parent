# trident-springboot-parent

#### 介绍

trident的父包，里面对依赖的jar包和build等进行了统一的版本管理，这样在微服务里写pom文件就会非常简洁了。

#### 安装教程

mvn clean install

#### 使用说明

```
    <parent>
        <groupId>x.trident</groupId>
        <artifactId>springboot-parent</artifactId>
        <version>1.0.0</version>
    </parent>
```

